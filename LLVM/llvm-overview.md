
- LVM介紹與簡單使用 | AI編譯器之傳統編譯
  https://www.youtube.com/playlist?list=PLuufbYGcg3p776cFHgF0KBVH0dyFOV6m_


- LLVM 教學
  
  <img src="doc/leture-1.png" width=50% height=auto>
  - https://blog.csdn.net/Zhanglin_Wu/article/details/124942823

- LLVM 教學系列影片
  - ep1- ep5 / 20 @ Dmitry Soshnikov
    
    https://www.youtube.com/playlist?list=PLGNbPb3dQJ_5Dv45KhMlt9HMSlnfMyb2V

  - Dmitry Soshnikov * 16 @ Haibo Yan
    
    https://www.youtube.com/watch?v=qMTMLfiAIqE&list=PLTsf9UeqkReaSg6PzJYXAA0cMQzrzMiTT

  - toy language tutorial with llvm
    - 官方doc，https://mlir.llvm.org/docs/Tutorials/Toy/
    - youtube * 16 @ StreetFighterTV ，https://www.youtube.com/playlist?list=PLYt1UWqccARtbTuFPDdQz1sHF6TRYFgdh

  - 如何使用LLVM和MLIR来构建一个编译器？（全20集）
    - from: https://www.youtube.com/watch?v=Ij4LswX1tZU&list=PLlONLmJCfHTo9WYfsoQvwjsa5ZB6hj
    - bilibili: https://www.bilibili.com/video/BV1h14y1J7Gm

  - LLVM新手演示教程（共25集）
    - from: https://www.youtube.com/watch?v=09EAVa7BAp4&list=PLSq9OFrD2Q3ChEc_ejnBcO5u9JeT0u
    - bilibili: https://www.bilibili.com/video/BV1U14y1E7zT