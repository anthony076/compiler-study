## 編譯係統概述

- Make 系列
  - makefile適合簡單專案，但不是合大型專案
  
  - 不同平臺的 makefile 有不同的規範和標準，不保證能跨平台編譯
    - GNU 的 make
    - QT 的 qmake
    - 微軟 的 MSnmake
    - BSD 的 pmake

- Ninja
  - 為了解決 make 執行速度慢的問題
  - 捨棄了各種高級功能，文法和用法簡單，編譯速度快

- Cmake
  - 為了解決 makefile 跨平台編譯的問題
  - cmake是makefile的上層工具，為了産生可移植的makefile
  - Cmake可以生成 .ninja 和.makefile 的工具

## Ref 

- [GCC、CMake、CMakelist、Make、Makefile、Ninja的關係](https://zhuanlan.zhihu.com/p/638986464)